const openButton = document.querySelector('#open');
const closeButton = document.querySelector('#close');
const modal = document.querySelector('.modal');
const modalOverlay = document.querySelector('.modal-overlay');

openButton.addEventListener('click', () => {
  modal.classList.toggle('closed');
  modalOverlay.classList.toggle('closed');
});
closeButton.addEventListener('click', () => {
  modal.classList.toggle('closed');
  modalOverlay.classList.toggle('closed');
});